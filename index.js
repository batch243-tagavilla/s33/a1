/*  */
const titleArr = [];

fetch("https://jsonplaceholder.typicode.com/todos")
    .then((response) => response.json())
    .then((json) => {
        json.map((obj) => {
            titleArr.push(obj.title);
        });
    });

console.log(titleArr);

/*  */
fetch("https://jsonplaceholder.typicode.com/todos/1")
    .then((response) => response.json())
    .then((json) => {
        console.log(json);
        console.log(
            `The item "${json.title}" on the list has a status of ${json.completed}`
        );
    });

/*  */
fetch("https://jsonplaceholder.typicode.com/todos", {
    method: "POST",
    headers: {
        "Content-type": "application/json",
    },
    body: JSON.stringify({
        userId: 1,
        title: "Created To Do List Item",
        completed: false,
    }),
})
    .then((response) => response.json())
    .then((json) => console.log(json));

/*  */
fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: {
        "Content-type": "application/json",
    },
    body: JSON.stringify({
        title: "Updated To Do List Item",
        description:
            "To update the my to do list with a different data structure",
        status: "Pending",
        dateCompleted: "Pending",
        userId: 1,
    }),
})
    .then((response) => response.json())
    .then((json) => console.log(json));

/*  */
fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PATCH",
    headers: {
        "Content-type": "application/json",
    },
    body: JSON.stringify({
        title: "delectus aut autem",
        status: "Complete",
        dateCompleted: "07/09/21",
        userId: 1,
    }),
})
    .then((response) => response.json())
    .then((json) => console.log(json));

/*  */
fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "DELETE",
});
